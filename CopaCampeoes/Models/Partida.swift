//
//  Partida.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 01/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

struct Partida: Decodable {
	
	let nomeFase: String
	let jogos: [Jogo]
	
	private enum CodingKeys: String, CodingKey {
		case nomeFase = "stage"
		case jogos = "games"
	}
}
