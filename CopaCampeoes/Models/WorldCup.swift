//
//  WorldCup.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 29/02/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

struct WorldCup: Decodable {
	
	let ano: Int
	let pais: String
	let vencedor: String
	let vice: String
	let vencedorPontuacao: String
	let vicePontuacao: String
	let partidas: [Partida]
	
	private enum CodingKeys: String, CodingKey {
		case ano = "year"
		case pais = "country"
		case vencedor = "winner"
		case vice = "vice"
		case vencedorPontuacao = "winnerScore"
		case vicePontuacao = "viceScore"
		case partidas = "matches"
	}
}
