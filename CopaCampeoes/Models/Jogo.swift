//
//  Jogo.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 01/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

struct Jogo: Decodable {
	
	let casa: String
	let convidado: String
	let placar: String
	let data: String
	
	private enum CodingKeys: String, CodingKey {
		case casa = "home"
		case convidado = "away"
		case placar = "score"
		case data = "date"
	}
}

//Se nao implementar o protocolo o seguinte erro aparecera
/*
The data couldn’t be read because it is missing.
*/
