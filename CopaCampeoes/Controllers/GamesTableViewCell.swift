//
//  GamesTableViewCell.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 08/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class GamesTableViewCell: UITableViewCell {
	
	@IBOutlet weak var ivHome: UIImageView!
	@IBOutlet weak var ivAway: UIImageView!
	@IBOutlet weak var lbHome: UILabel!
	@IBOutlet weak var lbAway: UILabel!
	@IBOutlet weak var lbScore: UILabel!
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
	func preparaCelula(com jogo: Jogo){
		self.ivHome.image = UIImage(named: jogo.casa)
		self.ivAway.image = UIImage(named: jogo.convidado)
		self.lbHome.text = jogo.casa
		self.lbAway.text = jogo.convidado
		self.lbScore.text = "\(jogo.placar)"
	}
	
}
