//
//  WinnersTableViewController.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 29/02/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class WinnersTableViewController: UITableViewController {
	
	var copasDoMudo: [WorldCup] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		carregarCopasDoMundo()
    }

	func carregarCopasDoMundo(){
		let fileUrl = Bundle.main.url(forResource: "winners", withExtension: "json")!
		let jsonData = try! Data(contentsOf: fileUrl)
		do {
			copasDoMudo = try JSONDecoder().decode([WorldCup].self, from: jsonData)
		}catch{
			print(error.localizedDescription)
		}
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc = segue.destination as? WorldCupViewController{
			let copa = copasDoMudo[self.tableView.indexPathForSelectedRow!.row]
			vc.copaDoMundo = copa
		}
	}
	
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // Quando retorna somente 1, tem a opcao de nao implementar esse metodo
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return copasDoMudo.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WorldCupTableViewCell
		
		let copaDoMundo = copasDoMudo[indexPath.row]
		cell.prepararCelula(com: copaDoMundo)
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
