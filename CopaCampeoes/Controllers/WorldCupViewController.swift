//
//  WorldCupViewController.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 29/02/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class WorldCupViewController: UIViewController {
	
	@IBOutlet weak var ivWinner: UIImageView!
	
	@IBOutlet weak var ivVice: UIImageView!
	
	@IBOutlet weak var lbScore: UILabel!
	
	@IBOutlet weak var lbVice: UILabel!
	@IBOutlet weak var lbWinner: UILabel!
	var copaDoMundo: WorldCup!
	
	@IBOutlet weak var tableView: UITableView!
	override func viewDidLoad() {
        super.viewDidLoad()
		if let copa = self.copaDoMundo {
			preparaDados(com: copa)
		}

    }
    
	func preparaDados(com copa: WorldCup){
		title = "Copa do mundo \(copa.ano)"
		ivWinner.image = UIImage(named: copa.vencedor)
		ivVice.image = UIImage(named: copa.vice)
		lbWinner.text = copa.vencedor
		lbVice.text = copa.vice
		lbScore.text = "\(copa.vencedorPontuacao) x \(copa.vicePontuacao) "
	}
}

extension WorldCupViewController: UITableViewDelegate {
	
}

extension WorldCupViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.copaDoMundo.partidas.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		//retona as fases representada pelo indice nessa secao
		let jogos = self.copaDoMundo.partidas[section].jogos
		return jogos.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GamesTableViewCell
		
		let partidas = self.copaDoMundo.partidas[indexPath.section]
		let jogo = partidas.jogos[indexPath.row]
		cell.preparaCelula(com: jogo)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		let partidas = self.copaDoMundo.partidas[section]
		return partidas.nomeFase
	}
	
}
