//
//  WorldCupTableViewCell.swift
//  CopaCampeoes
//
//  Created by Paulo Gutemberg on 07/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class WorldCupTableViewCell: UITableViewCell {

	@IBOutlet weak var lbYear: UILabel!
	
	@IBOutlet weak var ivWinner: UIImageView!
	@IBOutlet weak var ivVice: UIImageView!
	
	@IBOutlet weak var lbWinner: UILabel!
	@IBOutlet weak var lbVice: UILabel!
	@IBOutlet weak var lbWinnerScore: UILabel!
	
	@IBOutlet weak var lbViceScore: UILabel!
	@IBOutlet weak var lbCountry: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	func prepararCelula(com copa: WorldCup){
		self.lbYear.text = "\(copa.ano)"
		self.ivWinner.image = UIImage(named: "\(copa.vencedor).png")
		self.ivVice.image = UIImage(named: "\(copa.vice).png")
		self.lbWinner.text = copa.vencedor
		self.lbVice.text = copa.vice
		self.lbCountry.text = copa.pais
		
		self.lbWinnerScore.text = copa.vencedorPontuacao
		self.lbViceScore.text = copa.vicePontuacao
	}
}
